
import argparse


def parse_args():
    # Set base defaults
    defaults = dict()
    defaults["batch"] = "default"
    defaults["beep"] = False
    defaults["camera_interval"] = 0.05
    defaults["camera"] = False
    defaults["clobber"] = False
    defaults["loop_interval"] = 0.5
    defaults["period"] = 30.0
    defaults["port"] = "/dev/ttyUSB0"
    defaults["runs"] = 1
    defaults["sense_interval"] = 0.05
    defaults["sense"] = False
    defaults["speed"] = 50
    defaults["export_protocol"] = 'tcp'
    defaults["offline"] = False
    defaults["actor_log_dir"] = "./default/"
    defaults["stream_log_path"] = "./default/stream_observations.csv"
    defaults["sense_log_path"] = "./default/sense_observations.csv"

    # Read the config file if it exists
    try:
        from settings import settings
        defaults.update(settings)
    except ImportError:
        pass

    # Parse other command line arguments
    description = ("This program attempts to predict when the robot will next "
                   "bump into a wall.")
    parser = argparse.ArgumentParser(description=description)
    parser.set_defaults(** defaults)
    parser.add_argument("-b", "--batch",
                        help=("name of experiment batch; if default, "
                              "then clobber is automatically set"))
    parser.add_argument("-B", "--beep",
                        help="enable the beep prediction indicator",
                        action="store_true")
    parser.add_argument("-m", "--camera_interval",
                        help="interval between camera images in seconds",
                        type=float)
    parser.add_argument("-M", "--camera",
                        help="obtain images from an attached camera",
                        action="store_true")
    parser.add_argument("-C", "--clobber",
                        help="overwrite existing batch",
                        action="store_true")
    parser.add_argument("-l", "--loop_interval",
                        help="length of each loop interval in seconds",
                        type=float)
    parser.add_argument("-p", "--period",
                        help="period of a run in seconds",
                        type=float)
    parser.add_argument("-P", "--port",
                        help="port to interface with")
    parser.add_argument("-r", "--runs",
                        help="number of runs to do",
                        type=int)
    parser.add_argument("-e", "--sense_interval",
                        help=("interval between fetches from sense hat in "
                              "seconds"),
                        type=float)
    parser.add_argument("-E", "--sense",
                        help="obtain readings from an attached senseor hat",
                        action="store_true")
    parser.add_argument("-s", "--speed",
                        help="speed of movement for the robot",
                        type=int)
    parser.add_argument("-x", "--export_protocol",
                        help=("the protocol with which to export data - "
                              "either tcp (recommended) or websocket"))
    parser.add_argument("-F", "--offline",
                        help=("indicates that the actor should use offline "
                              "data"),
                        action="store_true")
    parser.add_argument("-a", "--actor-log-dir",
                        help=("the directory the actor log files are stored in "
                              "- used for offline learning"))
    parser.add_argument("-o", "--stream-log-path",
                        help="the path of the raw stream observations log")
    parser.add_argument("-S", "--sense-log-path",
                        help="the path of the raw sense observations log")
    args = vars(parser.parse_args())

    return args
