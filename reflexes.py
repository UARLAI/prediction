
class Reflexes:

    def __init__(self, control, control_lock):
        self.control = control
        self.control_lock = control_lock

    def run(self, observation):
        with self.control_lock:

            # Set leds appropriately
            if (observation["BumpsAndWheelDrops"] & 0b1111) == 0b1111:
                self.control.set_led(playOn=True,
                                     advOn=True,
                                     powColor=0,
                                     powIntensity=256)
            elif (observation["BumpsAndWheelDrops"] & 0b0011) == 0b0011:
                self.control.set_led(playOn=True,
                                     advOn=False,
                                     powColor=0,
                                     powIntensity=256)
            elif (observation["BumpsAndWheelDrops"] & 0b1100) == 0b1100:
                self.control.set_led(playOn=False,
                                     advOn=True,
                                     powColor=0,
                                     powIntensity=256)
            else:
                self.control.set_led(playOn=False,
                                     advOn=False,
                                     powColor=0,
                                     powIntensity=256)

            # Stop if trying to move forwards while bump sensors are on
            if (((observation["BumpsAndWheelDrops"] & 0b0011) == 0b0011) and
                    (observation["RightVelocity"] > 0) and
                    (observation["LeftVelocity"] > 0)):
                        self.control.drive_direct(0, 0)

            # Stop if trying to move backwards while wheel drop sensors are on
            if (((observation["BumpsAndWheelDrops"] & 0b1100) == 0b1100) and
                    (observation["RightVelocity"] < 0) and
                    (observation["LeftVelocity"] < 0)):
                        self.control.drive_direct(0, 0)
