
import sys
import json


class WebsocketExporter:

    def send(self, data):
        print(json.dumps(data), file=sys.stdout, flush=True)