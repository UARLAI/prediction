
from .tcp_exporter import TCPExporter
from .websocket_exporter import WebsocketExporter
from tools import get_ip

class RemoteExporter:
    """
    Export arbitrary data as a dictionary to a remote client.

    Args:
        protocol (str): Either "websocket" or "tcp". Otherwise, the send
            method does nothing.
    """

    def __init__(self, protocol=None):
        self.timestep = 0

        if protocol == 'websocket':
            self.exporter = WebsocketExporter()
        elif protocol == 'tcp':
            self.exporter = TCPExporter(host=get_ip())
        else:
            self.exporter = None

    def send(self, data):
        """
        Export data according to the protocol chosen at initialization.

        The current timestep is exported along with the data (as an additional
        entry in the data dictionary). If an invalid protocol, or no protocol, 
        was chosen at initialization, this method does nothing.

        Since the timestep must be incremented explicitly by calling
        increment_timestep, multiple packets of data can be sent on a single
        timestep simply by calling send multiple times before calling
        increment_timestep.

        Args:
            data (dict): An arbitrary collection of data. "timestep" should be
                avoided as a key name, as it will be overwritten.
        """
        if self.exporter:
            data['timestep'] = self.timestep
            self.exporter.send(data)
        else:
            pass

    def increment_timestep(self):
        """Increment the timestep."""
        self.timestep += 1
