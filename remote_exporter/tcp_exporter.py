
import json
import math
import multiprocessing
import socket
import sys
import threading
import time
from tools import get_ip


class TCPExporter(object):

    class __TCPExporter:

        def __init__(self, host, port):
            self.export_queue = multiprocessing.Queue()
            t = threading.Thread(target=self._connect_and_listen, 
                                 args=(host, port), 
                                 daemon=True)
            t.start()

        def _connect_and_listen(self, host, port):
            # Set up a socket
            server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            server_socket.bind((host, port))
            server_socket.listen(5)

            # Wait for a client; this is blocking, but since this function runs 
            # on a separate thread it doesn't interfere with the program that's 
            # doing the exporting in case there's no client
            self.conn, address = server_socket.accept()

            # Send data whenever it becomes available
            while True:
                self._send(self.export_queue.get())

        def _send(self, data):
            self.conn.send((json.dumps(data)+'\n').encode())

        def send(self, data):
            self.export_queue.put(data)

    # Singletonification
    _instance = None
    def __new__(cls, host='localhost', port=6789):
        if not TCPExporter._instance:
            TCPExporter._instance = TCPExporter.__TCPExporter(host, port)
        return TCPExporter._instance


# Demo
if __name__ == '__main__':

    tcp_exporter = TCPExporter(host='localhost')
    i = 0
    while True:
        tcp_exporter.send({'sensor_1': math.cos(math.pi*i/30),
                           'sensor_2': math.sin(math.pi*i/30),
                           'timestep': i})
        time.sleep(1)
        i += 1