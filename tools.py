
import collections
import os


def index_sum(indices, values):
    """Returns the sum of values[indices[i]] for each i in len(indices).
    """
    sum_ = 0
    for i in indices:
        sum_ += values[i]
    return sum_


def is_iterable(obj):
    """Returns true if an object is iterable otherwise False. For the
    purpose of this function, strings are not considered iterable.
    """
    try:
        iter(obj)
    except TypeError:
        return False
    else:
        return type(obj) != str


def locking(lock):
    """Decorator to acquire a lock prior to function execution and
    release the lock after execution.
    """
    def _locking(function):
        def wrapper(self, * args, ** kwargs):
            with lock:
                rv = function(self, * args, ** kwargs)
            return rv
        return wrapper
    return _locking


def scale(value, start_min, start_max, end_min, end_max):
    """Returns the result of scaling value from the range
    [start_min, start_max] to [end_min, end_max].
    """
    return (end_min + (end_max - end_min) *
            (value - start_min) / (start_max - start_min))


def destringify_number(str_):
    """Converts a str to an int or float, if appropriate."""
    try:
        if float(str_) == int(float(str_)):
            return int(float(str_))
        return float(str_)
    except ValueError:
        return str_

def get_ip():
    """Gets the IP address of the machine."""
    with os.popen('hostname -I') as hostname:
        return hostname.read()
