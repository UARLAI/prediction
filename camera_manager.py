
import cv2 as cv
import json
import os
import time
import warnings


class CameraManager:
    """Reads image data from an attached camera using open cv and adds
    it to an output stream. A new image is fetched every fetch_interval
    seconds.
    """
    def __init__(self, fetch_interval, output, image_size=26):
        self.fetch_interval = fetch_interval
        self.output = output
        self.video = cv.VideoCapture(0)
        if self.video.isOpened():
            self.video.set(cv.CAP_PROP_FRAME_HEIGHT,image_size)
            self.video.set(cv.CAP_PROP_FRAME_WIDTH,image_size)
        else:
            raise RuntimeError("no camera found")

    def run(self, halt_flag):
        """Begin the stream input processing loop."""
        observation_id = 0
        first_run = True

        # Write opening bracket for array in outfile
        with open("image_observations.json", "w") as outfile:
            outfile.write("[")

        while not halt_flag.is_set():
            success, image = self.video.read()
            if not success:
                continue
            image = cv.resize(image, (26,26))

            observation_id += 1
            observation_time = time.time()
            observation = {'observation_id': observation_id,
                           'observation_time': observation_time,
                           'image': image}

            # Log observation
            with open("image_observations.json", "a") as outfile:
                if first_run:
                    first_run = False
                else:
                    outfile.write(", ")
                json.dump({'observation_id': observation_id,
                           'observation_time': observation_time,
                           'image': image.tolist()}, outfile)

            self.output.put(observation)

            # Warn the user if too much time has passed
            if time.time() - observation_time > self.fetch_interval:
                warnings.warn("image data handling time exceeds threshold")

            time.sleep(max(0,
                       self.fetch_interval - (time.time() - observation_time)))
        else:
            with open("image_observations.json", "a") as outfile:
                outfile.write("]")


def start_camera_manager(flag, fetch_interval, output):
    """Creates and starts a new CameraManager."""
    try:
        CameraManager(fetch_interval, output).run(flag)
    finally:
        flag.set()
