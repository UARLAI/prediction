# Get consistent hashing
import random
random.seed(0)
from RLtoolkit.Tiles.tiles import tiles
import itertools
import numpy as np
import math
import csv
import os, sys
from functools import reduce

class TileCoder:
    """
    sensor_groups: list of lists of indices of sensors to group together, indices here correspond to indices in
    the list passed to get_tiling later

    tiling_groups: list of tilings for each group
    each tiling is a 3 tuple of (dim, num_tiles, num_tilings)

    sensor_range: (min,max) possible value for each sensor expected in input to get_tiling.

    memory_size: indexes to use for each individual group 

    bias: presense of single tile that is always active

    fine_groupings: if true each tiling of sensors will be mapped to its own seperate group of tile indices
    if false tilings of the same sensor group and same dimension will be mapped to the same set of tile indices
    """
    def __init__(self,
                 sensor_groups,
                 tiling_groups,
                 sensor_ranges,
                 memory_size=4096,
                 bias=False,
                 fine_grouping=False):
        assert(len(sensor_groups)==len(tiling_groups))
        #initialize object attributes
        self.memory_size = memory_size
        self.sensor_groups = sensor_groups
        self.tiling_groups = tiling_groups
        self.sensor_min, self.sensor_max = zip(*sensor_ranges)
        self.sensor_ranges = [b-a for (a,b) in sensor_ranges]
        self.bias = bias
        self.theta_index = 0
        self.count = 0
        self.fine_grouping = fine_grouping
        sensor_group_sizes = list(map(len,sensor_groups))

        #compute num_tilings based on number of combinations in each group and number of tilings assigned
        #to each such combination
        self.num_tilings = 0
        for i in range(len(tiling_groups)):
            for tiling in tiling_groups[i]:
                self.num_tilings += self.binomial_coef(sensor_group_sizes[i], tiling[0])*tiling[2]
        if(bias):
            self.num_tilings+=1

        #compute number of indices needed by weight vector theta (theta_size), different depending on
        #whether groups are coarse or fine
        if(fine_grouping):
            self.theta_size = 0
            for i in range(len(tiling_groups)):
                for tiling in tiling_groups[i]:
                # Note: tiling[0] returns the number of dimensions in the tiling
                    self.theta_size += memory_size* self.binomial_coef(sensor_group_sizes[i], tiling[0])
        else:
            self.theta_size = memory_size*sum(map(len,tiling_groups))
        if bias:
            self.theta_size+=1



    def binomial_coef(self, n,k):
        f = math.factorial
        product = lambda a, b: a * b
        return int(reduce(product, range(k, n+1)) / f(n-k))

    def get_tiling(self, state):
        """
        Get active tile indices for the given state (list of numbers).
        """
        ret = []
        self.count +=1
        tiling_index = 0
        #loop over all groups of sensors we wish to tile
        for sensor_group, tilings in zip(self.sensor_groups, self.tiling_groups):
            #for each group loop over each seperate tiling we wish to generate
            for tiling in tilings:
                #unpack the tiling into it's component specifications
                dim, num_tiles, num_tilings = tiling
                #loop over every combination of a specified tiling dimension for the current sensor group
                hashing_index = 0
                for group_indices in itertools.combinations(range(len(sensor_group)), dim):
                    #use tiling library to compute a tiling for the combination in question
                    normalize = lambda j: (state[j]-self.sensor_min[j])*num_tiles/self.sensor_ranges[j]
                    local_tiles = tiles(num_tilings,
                                        self.memory_size,
                                        [normalize(j) for j in [sensor_group[i] for i in group_indices]],
                                        [hashing_index])
                    #add the local tiles with an offset to map each group of tiles to a different index space
                    ret+=[tiling_index*self.memory_size + x for x in local_tiles]
                    if(self.fine_grouping):
                        tiling_index+=1
                    else:
                        hashing_index+=1
                if(not self.fine_grouping):
                    tiling_index+=1
        #add constant bias tiling at very end if requested
        if(self.bias):
            ret.append(tiling_index*self.memory_size)
        return ret


    def print_weight_statistics(self, batch, theta,full_tiles,time):
        group_count = 1
        start_indx = 0
        end_indx = 0

        for sensor_group,tilings in zip(self.sensor_groups,self.tiling_groups):
            col = []
            theta_list = []
            tiles_list = []
            tiles_theta = []

            ts = 'theta'+str(group_count)+'.csv'
            fts = 'full_tiles'+str(group_count)+'.csv'
            fp = lambda x: os.path.join(batch, x)
            opn = lambda x: open(fp(x), 'w')
            withadd = lambda x: open(fp(x), 'a')

            with opn(ts) as theta_stat, opn(fts) as full_tiles_stat:

                
                print(".. Group sensors : ",group_count)
                for tiling in tilings:

                    dim, num_tiles, num_tilings = tiling
                    col.append(dim)
                    if self.fine_grouping:
                        print(".. Group sensors: %d , Dimension : %d" % (group_count,dim))
                        end_indx += self.binomial_coef(len(sensor_group), dim)
                        print("max: " + str(np.max(np.absolute(theta[ start_indx * self.memory_size :end_indx * self.memory_size]))))
                        print("mean: " + str(np.mean(np.absolute(theta[ start_indx * self.memory_size :end_indx * self.memory_size]))))
                        ##
                        tiles_ = full_tiles[ start_indx * self.memory_size :end_indx * self.memory_size]
                        theta_ = theta[ start_indx * self.memory_size :end_indx * self.memory_size]
                        theta_list.append(theta_)

                        listmap = list(map(lambda x: x/time, full_tiles[ start_indx * self.memory_size :end_indx * self.memory_size]))
                        tiles_list.append(listmap)

                        tiles_theta.append((np.ones(len(theta_))*theta_*tiles_).tolist())
                       
                        start_indx = end_indx


                    else:
                        print(".. Group sensors: %d , Dimension : %d" % (group_count,dim))
                        end_indx += 1
                        print("max: " + str(np.max(np.absolute(theta[ start_indx * self.memory_size :end_indx * self.memory_size]))))
                        print("mean: " + str(np.mean(np.absolute(theta[ start_indx * self.memory_size :end_indx * self.memory_size]))))
                        ###
                        tiles_ = full_tiles[ start_indx * self.memory_size :end_indx * self.memory_size]
                        theta_ = theta[ start_indx * self.memory_size :end_indx * self.memory_size]
                        theta_list.append(theta_)

                        listmap = list(map(lambda x: x/time, full_tiles[ start_indx * self.memory_size :end_indx * self.memory_size]))
                        tiles_list.append(listmap)

                        tiles_theta.append((np.ones(len(theta_))*theta_*tiles_).tolist())
                        
                        ####

                        start_indx = end_indx
                        ###

                theta_stat.write(",".join([str(x) for x in col]))
                full_tiles_stat.write(",".join([str(x) for x in col]))

            with withadd(ts) as theta_stat, withadd(fts) as full_tiles_stat:    
                for i in range(len(theta_list[0])):
                    theta_stat.write(','.join([str(theta_list[j][i]) for j in range(len(theta_list))])+'\n')
                        
                for i in range(len(tiles_list[0])):
                    full_tiles_stat.write(','.join([str(tiles_list[j][i]) for j in range(len(tiles_list))])+'\n')

            group_count+=1

            if self.bias:
                print(".. Bias tile ..")
                end_indx += 1
                print("max: " + str(np.max(np.absolute(theta[ start_indx * self.memory_size :end_indx * self.memory_size]))))
                print("mean: " + str(np.mean(np.absolute(theta[ start_indx * self.memory_size :end_indx * self.memory_size]))))
                print("this is theta ... ", theta)
                print ("this is count ...", self.count)
