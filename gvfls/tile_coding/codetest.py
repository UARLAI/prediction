#from cliff_sensor_tiling import get_tiling
import random as rand
rand.seed(0)
from tile_coder import TileCoder
from collections import namedtuple
import time
import numpy as np
import math



CliffL = int(rand.random()*4096)
CliffFL = int(rand.random()*4096)
CliffFR = int(rand.random()*4096)
CliffR = int(rand.random()*4096)
print("cliff", CliffL, CliffFL, CliffFR, CliffR)
State = [[CliffL, CliffFL, CliffFR, CliffR]]
State = np.asarray(State)

'''State = namedtuple("State",
                   ["CliffLeftSignal",
                    "CliffFrontLeftSignal",
                    "CliffFrontRightSignal",
                    "CliffRightSignal",
                    "BumpSignal"])
State.CliffLeftSignal = CliffL
State.CliffFrontLeftSignal = CliffFL
State.CliffFrontRightSignal = CliffFR
State.CliffRightSignal = CliffR
print "this is State",CliffL
'''
'''
t_start = time.time()
print(get_tiling(State))
print("Time for simple tiling: "+str(time.time()-t_start))

#test tile coder uninterpretable version
tiler = tile_coder(
	memory_size = 4096,
	tilings_1 = 4,
	tiles_1 = 8,
	tilings_2 = 4,
	tiles_2 = 8,
	tilings_3 = 4,
	tiles_3 = 8,
	tilings_4 = 0,
	tiles_4 = 0,
	bias = True)

t_start = time.time()
tiles = tiler.get_tiling(State)
print(tiles)
assert(len(tiles)==4*tiler.tilings_1+6*tiler.tilings_2+4*tiler.tilings_3+tiler.tilings_4+tiler.bias)
print("Time for complex tiling: "+str(time.time()-t_start))'''
'''
#test tile coder with coarse_groupings and bias
tiler = tile_coder(sensor_groups=[[0,1,2,3]],
                           tiling_groups=
                            [
                              [
                                (1,8,4),
                                (2,8,4),
                                (3,8,4),
                                (4,8,4)
                              ]
                            ],
                           sensor_maxima =
                            [4095.0,
                             4095.0,
                             4095.0,
                             4095.0
                            ],
                           memory_size=4096,
                           bias=True,
                           fine_grouping=False)


tiles = tiler.get_tiling([300,400,500,600])
print(tiles)
print(len(tiles))
print(tiler.num_tilings)
assert(len(tiles)==tiler.num_tilings)
print(max(tiles))
print(tiler.theta_size)
assert(max(tiles)<tiler.theta_size)
theta = [1]*tiler.theta_size
tiler.print_weight_statistics(theta)

#test tile coder with coarse_groupings and no bias
tiler = tile_coder(sensor_groups=[[0,1,2,3]],
                           tiling_groups=
                            [
                              [
                                (1,8,4),
                                (2,8,4),
                                (3,8,4),
                                (4,8,4)
                              ]
                            ],
                           sensor_maxima =
                            [4095.0,
                             4095.0,
                             4095.0,
                             4095.0
                            ],
                           memory_size=4096,
                           bias=False,
                           fine_grouping=False)


tiles = tiler.get_tiling([300,400,500,600])
print(tiles)
print(len(tiles))
print(tiler.num_tilings)
assert(len(tiles)==tiler.num_tilings)
print(max(tiles))
print(tiler.theta_size)
assert(max(tiles)<tiler.theta_size)
theta = [1]*tiler.theta_size
tiler.print_weight_statistics(theta)'''

#test tile coder with fine_groupings and bias
tiler = TileCoder(sensor_groups=[[0,1,2,3]],
                           tiling_groups=
                            [
                              [
                                (1,20,8),
                                (2,20,8),
                                (3,20,8),
                                (4,20,8)
                              ]
                            ],
                           sensor_ranges =
                            [(0.0, 2000.0),
                             (0.0, 2000.0),
                             (0.0, 2000.0),
                             (0.0, 2000.0)
                            ],
                           memory_size=4096,
                           bias=True,
                           fine_grouping=False)




tiler.plot()

'''#test tile coder with fine_groupings and no bias
tiler = tile_coder(sensor_groups=[[0,1,2,3]],
                           tiling_groups=
                            [
                              [
                                (1,8,4),
                                (2,8,4),
                                (3,8,4),
                                (4,8,4)
                              ]
                            ],
                           sensor_maxima =
                            [4095.0,
                             4095.0,
                             4095.0,
                             4095.0
                            ],
                           memory_size=4096,
                           bias=False,
                           fine_grouping=True)


tiles = tiler.get_tiling([300,400,500,600])
print(tiles)
print(len(tiles))
print(tiler.num_tilings)
assert(len(tiles)==tiler.num_tilings)
print(max(tiles))
print(tiler.theta_size)
assert(max(tiles)<tiler.theta_size)
theta = [1]*tiler.theta_size
tiler.print_weight_statistics(theta)
'''
