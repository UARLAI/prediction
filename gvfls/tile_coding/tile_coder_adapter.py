
from .tile_coder import TileCoder


class TileCoderAdapter:
    """
    Adapts tile coder to take arbitrary state information.
    Should preform any nessesary computation to create the
    list of numbers expected by the tile_coder from whatever
    raw state information we want to pass.
    """
    def __init__(self,
                 sensor_groups,
                 tiling_groups,
                 sensor_ranges,
                 tiling_features,
                 bin_feature_groups=[],
                 memory_size=4096,
                 bias=False,
                 fine_grouping=False):
        self.tile_coder = TileCoder(sensor_groups,
                                    tiling_groups,
                                    sensor_ranges,
                                    memory_size,
                                    bias,
                                    fine_grouping)

        self.tiling_features = tiling_features

        # for each binary feature group, find the number of indices it
        # requires and put it in the extra list
        self.bin_feature_groups = bin_feature_groups
        num_indices = lambda g: math.pow(2, len(g)) if len(g) else 0
        extra = list(map(num_indices, bin_feature_groups))

        # determine the offset for each binary group
        offset = lambda i: extra[i] + (extra[i-1] if i else 0)
        self.offsets = [offset(i) for i in range(len(extra))]

        # get attributes from tile_coder and extend theta to accomodate
        # binary feature groups which we don't want to tile code
        self.num_tilings = self.tile_coder.num_tilings
        self.theta_size = self.tile_coder.theta_size + sum(extra)


    def binary(self, t):
        index = 0
        for i in range(len(t)):
            index += math.pow(2, i) if t[i] else 0
        return index

    def get_tiling(self, state):
        tiling_input = [state[feature] for feature in self.tiling_features]
        bin_groups = [[state[f] for f in g] for g in self.bin_feature_groups]
        bin_indices = list(map(self.binary, bin_groups))

        indices = []
        original_size = self.tile_coder.theta_size
        for i in range(len(bin_indices)):
            indices.append(original_size + self.offsets[i]+bin_indices[i])

        tilings = self.tile_coder.get_tiling(tiling_input)

        return tilings + indices

    def print_weight_statistics(self, batch, theta, full_tiles,time):
        self.tile_coder.print_weight_statistics(batch, theta, full_tiles,time)

    