#!/usr/bin/env python

import numpy as np
import unittest

from BinaryGTD import BinaryGTD


class TestBinaryGTD(unittest.TestCase):

    def test_update(self):
        theta = np.array([1.0, 2.0])
        traces = np.array([[0.1, 0.2]])
        phi_prime = np.array([0, 1])
        phi = np.array([1])
        w = np.array([[0.3, -0.1]])
        last_gamma = 0.1

        learner = BinaryGTD(alpha = 0.1, beta = 0.01, lambda_ = 0.6, gamma = 0.75, theta = theta, phi = phi)
        learner._e = traces
        learner._w = w
        learner.old_gamma = np.array(last_gamma)
        learner.old_lambda = np.array(0.6)

        learner.update(phi_prime, 0.05, rho=0.1)
        self.assertTrue(np.allclose([0.0006, 0.1012], traces))
        self.assertTrue(np.allclose(learner._phi, phi_prime))
        self.assertTrue(np.allclose([1.0003162, 2.0033342], learner.theta))
        self.assertTrue(np.allclose([0.3000018, -0.0986964], learner._w))

        phi = np.random.randint(0, 5, (3))
        theta = np.zeros((10,5))
        learner = BinaryGTD(0.1, 0.01, 0.9, 0.9, theta, phi)
        phi = np.random.randint(0, 5, (3))
        r = np.random.randint(0, 10, (10))
        rho = np.random.sample((10))
        learner.update(phi, r, rho)

        theta = np.zeros((10,5))
        phi = np.random.randint(0, 5, (3))
        single_learners = [BinaryGTD(0.1, 0.01, 0.9, 0.9, np.copy(theta[i,:]), phi) for i in range(5)]
        multi_learner = BinaryGTD(0.1, 0.01, 0.9, 0.9, np.copy(theta), phi)
        for i in range(100):
            phi = np.random.randint(0, 5, (3))
            r = np.random.randint(0, 10, (10))
            rho = np.random.sample((10))
            gamma = np.random.sample((10))
            alpha = np.random.sample((10))
            beta = np.random.sample((10))
            lambda_ = np.random.sample((10))
            for j in range(len(single_learners)):
                single_learners[j].update(phi, r[j], rho[j], alpha = alpha[j], beta = beta[j], lambda_ = lambda_[j], gamma = gamma[j])
            multi_learner.update(phi, r, rho, alpha = alpha, beta = beta, lambda_ = lambda_, gamma = gamma)
        for i in range(len(single_learners)):
            self.assertTrue(np.allclose(single_learners[i].theta, multi_learner.theta[i,:]))
            phi = np.random.randint(0, 5, (3))
            self.assertAlmostEqual(single_learners[i].predict(phi)[0], multi_learner.predict(phi)[i])

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestBinaryGTD)
    unittest.TextTestRunner(verbosity=2).run(suite)
