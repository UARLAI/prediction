import numpy as np
from functools import reduce
from operator import add
from itertools import combinations as comb

from plot_eval import shift_avg,
                      get_outcomes,
                      get_sq_error,
                      get_ema,
                      rsquared,
                      expectation
from tile_coding.tile_coder import TileCoder
from tile_coding.tile_coder_adapter import TileCoderAdapter

class EvalTilings:

    def __init__(self, file):

        self.ranges = 
        # "name" : (on/off, sensor_range)
                        {"WheeldropCaster": (0, 1), 
                         "WheeldropLeft": (0, 1), 
                         "WheeldropRight": (0, 1), 

                         "BumpLeftSignal": (0, 1), 
                         "BumpRightSignal": (0, 1), 

                         "Distance": (-25*1.1, 25*1.1), 
                         "Angle": (-5.96831*1.1, 5.96831*1.1), 

                         "IRByte": (0, 255),                           
                         "WallSignal": (0.0, 4095.0),  
                         "VirtualWall": (0, 1), 

                         "CliffLeftSignal": (0.0, 4095.0), 
                         "CliffFrontLeftSignal": (0.0, 4095.0), 
                         "CliffFrontRightSignal": (0.0, 4095.0), 
                         "CliffRightSignal": (0.0, 4095.0), 

                         "BatteryCharge": (0, 65535), 
                         "HomeChargingSourcesAvailable": (0, 1), 
                         "InternalChargingSourcesAvailable": (0, 1) 
                         }

        self.var_groups = ((1, "WheeldropRight", "WheeldropLeft", "WheeldropCaster"),
                           (1, "BumpRightSignal", "BumpLeftSignal"),
                           (0, "IRByte", "WallSignal", "VirtualWall"),
                           (1, "CliffFrontLeftSignal", "CliffRightSignal", "CliffFrontRightSignal", "CliffLeftSignal"),
                           (1, "BatteryCharge", "HomeChargingSourcesAvailable", "InternalChargingSourcesAvailable"))
        
        self.read_csv(file)

        for sensors, sensor_groups, ranges in self.sensor_group_generator():
            for tiles in self.tiling_group_generator(sensor_groups):
                # tiler = TileCoderAdapter(TileCoder(sensor_groups=[[0, 1, 2, 3]],
                #                                    tiling_groups=[[(1, 8, 4),
                #                                                    (2, 8, 4),
                #                                                    (3, 8, 4),
                #                                                    (4, 8, 4)]],
                #                                    sensor_ranges=[(0,4095.0),
                #                                                   (0,4095.0),
                #                                                   (0,4095.0),
                #                                                   (0,4095.0)],
                #                                    memory_size=4096,
                #                                    bias=True,
                #                                    fine_grouping=False))
                tiler = TileCoderAdapter(TileCoder(sensor_groups=sensor_groups,
                                                   tiling_groups=tiles,
                                                   sensor_ranges=ranges,
                                                   memory_size=4096,
                                                   bias=True,
                                                   fine_grouping=False))

    def sensor_group_generator(self):
        size = sum([1 for i in self.var_groups if i[0]])
        listsum = lambda x, y: x[1:] + y[1:]
        valids: [group for group in self.var_groups if group[0]]

        for i in range(size):
            for j in comb(range(size), i):
                yield [list(reduce(listsum, valids))],
                      [j], 
                      [self.ranges[sensor] for sensor in valids]

    def tiling_group_generator(self, sensor_groups):
        
        def single_group_generator(sensor_group):
            binaryp = lambda i: self.ranges[sensor_group[i]] == (0,1)
            tile_choice = lambda i: (i, 2, 1) if binaryp(i+1) else (i, 8, 4)
            return [tile_choice(i) for i in range(len(sensor_group[1:]))]

        return list(map(single_group_generator, sensor_groups))

    def read_csv(self, csv_file):

        raw_csv = np.loadtxt(csv_file, delimiter=',', dtype=str)

        self.n_features = sum([len(g)-1 for g in self.var_groups if g[0]])

        eval_data, state_data = np.split(raw_csv, [8], 1)

        i = 0
        while i < len(state_data):
          if [g[0] for g in self.var_groups if [state_data[0,i] in g][0]:
               state_data = np.delete(state_data, i, 1)
          else: 
               i += 1
        self.state_data = np.delete(state_data, 0, 0).astype(np.float)

        eval_data = np.delete(eval_data, range(2), 0).astype(np.float)
        self.eval_data_list = np.split(eval_data, len(eval_data)/4, 1)

    def get_measures(self, predictions, use_online_predictions=False):

        def helper():
            predictions = [data[:,0]] if use_online_predictions else [predictions]
            continuations = [data[:,2]]
            cumulants = [data[:,1]]
            sq_avg_norm_td = [data[:,3]]

            outcomes = get_outcomes(continuations, cumulants)
            ema_outcomes = get_ema(outcomes)

            naive_sq_err = get_sq_err(ema_outcomes, outcomes)
            ema_naive_sq_err = get_ema(naive_sq_err)[0]

            sq_err = get_sq_err(predictions, outcomes)
            ema_sq_err = get_ema(sq_err)[0]

            expected = expectation(outcomes[0])
            sq_deviation = lambda x: math.pow(x-expected, 2)
            naive_ema_sq_err = get_ema(list(map(sq_deviation, outcomes[0])))
            ema_r_sq = 1 - ema_sq_err[-1]/naive_ema_sq_err[-1]
            r_sq = rsquared(sq_err[0], outcomes[0])

            n_samples = len(outcomes[0])
            adjust = lambda x: 1 - (1 - x) * (n_samples - 1) / (n_samples - self.n_features)

            a_ema_r_sq = adjust(ema_r_s)
            a_r_sq = adjust(r_sq)

            return ema_sq_err[-1], ema_naive_sq_err[-1], r_sq, a_r_sq, ema_r_sq, a_ema_r_sq

        return list(map(helper, self.eval_data_list))

