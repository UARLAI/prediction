
import math
import numpy as np
import time

from logs.csv_logger import CSVLogger


class Evaluator():

    def __init__(self,
                 gvf_number,
                 batch="default",
                 run=0,
                 pred_id=0,
                 clobber=False):
        """
        Constructs a new object with the given parameters.
        """
        self.eval_log = CSVLogger(batch,
                                  'prediction_{}_run_{}'.format(pred_id, run),
                                  clobber,
                                  number=True)

        self.gvf_number = gvf_number
        self.sum_squared_td_error = np.zeros(gvf_number)
        self.targets = np.zeros((gvf_number, 40000))
        self.rho_list = np.zeros((gvf_number, 40000))
        self.time_step = 0

    def update(self, cumulant, gamma, rho, V, V_prime):

        # compute td error
        td_error = cumulant + gamma * V_prime - V
        squared_td_error = td_error * td_error * rho
        self.sum_squared_td_error = self.sum_squared_td_error + squared_td_error
        # compute target
        self.targets[:, self.time_step] = (cumulant + gamma * V_prime)
        self.rho_list[:, self.time_step] = rho
        self.time_step = self.time_step + 1


    # compute final score over all the data
    def compute_score(self):
        self.targets = self.targets[:, 0 : self.time_step + 1]
        self.rho_list = self.rho_list[:, 0 : self.time_step + 1]
        scores = np.zeros(self.gvf_number)
        for i in range(self.gvf_number):
            mean_target = np.dot(self.targets[i, :], self.rho_list[i, :]) / np.sum(self.rho_list[i, :])
            target_distance = self.targets[i, :] - mean_target
            squared_target_distance = target_distance * target_distance
            denominator = np.dot(squared_target_distance, self.rho_list[i, :])
            scores[i] = self.sum_squared_td_error[i] / denominator
        return scores

    def log_evaluation_results(self, prediction, cumulant, gamma, td_variation, null=False):
        """
        Writes timestep data to a file.
        """
        if null:
            self.eval_log.write(['NA','NA','NA','NA'])
        else:
            self.eval_log.write([prediction,
                                 gamma,
                                 cumulant,
                                 td_variation])
