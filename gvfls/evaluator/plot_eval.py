from functools import reduce
from os import listdir
from os.path import isfile, join
import argparse
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

def parse_args():
    parser = argparse.ArgumentParser(description=("Plots fields from the " +
                                                  "evaluator's CSV logs. " +
                                                  "Averages the readings if " +
                                                  "more than one run exists."))
    parser.add_argument("-d", "--directory",
                        help="Directory containing the CSVs.",
                        type=str)
    parser.add_argument("-r","--runs",
                        help="Run numbers of those to be analyzed. If not " +
                             "included, all runs will be included.",
                        nargs='+',
                        default=0,
                        type=int)
    parser.add_argument("-i","--pred_id",
                        help="Prediction ID to be plotted",
                        default="default",
                        type=str)
    parser.add_argument("-e",
                        action="store_true",
                        help="Plot Errors.")
    parser.add_argument("-m",
                        action="store_true",
                        help="Plot Moving Average Squared Prediction Error.")
    parser.add_argument("-p",
                        action="store_true",
                        help="Plot Predictions.")
    parser.add_argument("-t",
                        action="store_true",
                        help="Plot TD Error Statistics.")
    parser.add_argument("-s",
                        action="store_true",
                        help="Print Amount of Variance Explained (R^2)")

    return parser.parse_args()

def lstavg_maker(n):

    def lstavg(lst1, lst2):
        lst3 = []
        for i in range(len(l1)):
            lst3.append(lst1[i]/n+lst2[i]/n)
        return lst3

    return lstavg

def expectation(lst):
    return sum(lst)/len(lst)

def variance(lst):
    expected = expectation(lst)
    sq_error = list(map(lambda x: math.pow(x-expected,2), lst))
    return expectation(sq_error)

def rsquared(sq_error, outcome):
    expected = expectation(outcome)
    sq_deviation = lambda x: math.pow(x-expected, 2)
    return 1 - sum(sq_error)/sum(map(sq_deviation, outcome))

def get_outcomes(continuations, cumulants):
    outcomes = [[]]

    for i in range(len(cumulants)):
        outcomes[i].insert(0, 0)

        for t in range(1,len(cumulants[i]))[::-1]:
            outcomes[i].insert(0, outcomes[i][0]*continuations[i][t]+cumulants[i][t])

    return outcomes

def get_sq_err(predictions, targets):

    sq_err = [[]]

    for i in range(len(targets)):
        for t in range(len(targets[i])):
            sq_err[i].append(math.pow(predictions[i][t] - targets[i][t], 2))

    return sq_err

def shift_avg(average, update, weight=0.01):
    return (1-weight)*average + weight*update

def get_ema(data):

    ema = [[]]

    for i in range(len(data)):
        avg = 0
        scale = 0
        for t in range(len(data[i])):
            avg = shift_avg(avg, data[i][t])
            scale = shift_avg(scale, 1)
            ema[i].append(avg/scale)

    return ema

if __name__ == "__main__":
    args = parse_args()
    fig, ax = plt.subplots()
    path = args.directory

    add_path = lambda f: join(path,f)
    load = lambda f: np.loadtxt(f, delimiter=',')
    pred_tag = "prediction_{}".format(args.pred_id)

    if args.runs:
        valids = [add_path(pred_tag+"_run_"+i+".csv") for i in args.runs]
    else:
        valids = [add_path(pred_tag+"_run_0.csv")]

    check = lambda f: f in valids

    # data is a list of numpy arrays each containing data from one run
    data = list(map(load, filter(check, map(add_path, listdir(path)))))

    if not data:
        exp_str = "File not found.\nLooking for {}".format(valids)
        raise Exception(exp_str)

    # time
    if len(data[0][:,0]) < 3000:
        # if run for less than 5 minutes display seconds
        xx = list(map(lambda x: x/10, data[0][:,0]))
        ax.set_xlabel("Time (s)")
    else:
        # otherwise display minutes
        xx = list(map(lambda x: x/600, data[0][:,0]))
        ax.set_xlabel("Time (min)")


    # process the csv
    predictions = [run[:,1] for run in data]
    continuations = [run[:,2] for run in data]
    cumulants = [run[:,3] for run in data]
    sq_avg_norm_td = [run[:,4] for run in data]

    outcomes = get_outcomes(continuations, cumulants)
    ema_outcomes = get_ema(outcomes)
    naive_sq_err = get_sq_err(ema_outcomes, outcomes)
    sq_err = get_sq_err(predictions, outcomes)
    ema_naive_sq_err = get_ema(naive_sq_err)
    ema_sq_err = get_ema(sq_err)

    # pm = lambda y: [print(min(x),max(x)) for x in y]
    # pm(outcomes)
    # pm(continuations)
    # pm(cumulants)

    ax.set_xlim([0,xx[-1]])

    if args.p: # predictions
        yy = reduce(lstavg_maker(len(data)), predictions)

        ax.scatter(xx, yy, 'g', linewidth=0.15, alpha=0.4, s=5)

        lim = max(yy)
        ax.set_ylim([-lim*0.05,lim*1.25])
        ax.set_title("Outcome Predictions")
        ax.set_ylabel("Predictions")
        plt.show()

    if args.e: # error
        # plot naive squared error (prediction is average of past outcomes)

        zz = reduce(lstavg_maker(len(data)), naive_sq_err)
        yy = reduce(lstavg_maker(len(data)), sq_err)

        # plot squared error
        ax.scatter(xx, zz, color='b',linewidth=0.15, alpha=0.4, s=5, label="EMA of Past Outcomes as Predictions")
        ax.scatter(xx, yy, color='g',linewidth=0.15, alpha=0.4, s=5, label="Robot Predictions")

        lim = max(yy+zz)
        ax.set_ylim([-lim*0.05,lim*1.25])
        ax.legend()
        ax.set_title("Squared Prediction Error")
        ax.set_ylabel("Squared Error")
        plt.show()

    if args.m: # exponential moving average of squared error
        zz = reduce(lstavg_maker(len(data)), ema_naive_sq_err)
        yy = reduce(lstavg_maker(len(data)), ema_sq_err)

        ax.plot(xx, zz, 'b', linewidth=0.6, label="EMA of Past Outcomes as Predictions")
        ax.plot(xx, yy, 'g', linewidth=0.6, label="Robot Predictions")

        ax.legend()
        lim = max(yy+zz)
        ax.set_ylim([-lim*0.05,lim*1.25])
        ax.set_title("Exponential Moving Average of Squared Prediction Error")
        ax.set_ylabel("Squared Error")
        plt.show()

    if args.t: # TD error Statistics
        yy = reduce(lstavg_maker(len(data)), sq_avg_norm_td)
        ax.plot(xx, yy, color='g',linewidth=0.6)

        ax.set_ylim([min(yy)*0.05,max(yy)*1.25])
        ax.set_title("Stability of Learning")
        ax.set_ylabel("TD Error Statistic")
        plt.show()

    for i in (args.runs if args.runs else range(1)):
        print("R^2 for run {}: {}".format(i, rsquared(sq_err[i], outcomes[i])))
