
gvfs = {
    "all_cliff_threshold_action_backward_1": {
        "cumulant": (lambda observation: int(
                        observation["CliffFrontRightSignal"] <= 500 and
                        observation["CliffFrontLeftSignal"] <= 500 and
                        observation["CliffRightSignal"] >= 900 and
                        observation["CliffLeftSignal"] >= 900)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "backward"))
    },
    "all_cliff_threshold_action_forward_1": {
        "cumulant": (lambda observation: int(
                        observation["CliffFrontRightSignal"] <= 500 and
                        observation["CliffFrontLeftSignal"] <= 500 and
                        observation["CliffRightSignal"] >= 900 and
                        observation["CliffLeftSignal"] >= 900)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "forward"))
    },
    "all_cliff_threshold_action_left_1": {
        "cumulant": (lambda observation: int(
                        observation["CliffFrontRightSignal"] <= 500 and
                        observation["CliffFrontLeftSignal"] <= 500 and
                        observation["CliffRightSignal"] >= 900 and
                        observation["CliffLeftSignal"] >= 900)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "left"))
    },
    "all_cliff_threshold_action_right_1": {
        "cumulant": (lambda observation: int(
                        observation["CliffFrontRightSignal"] <= 500 and
                        observation["CliffFrontLeftSignal"] <= 500 and
                        observation["CliffRightSignal"] >= 900 and
                        observation["CliffLeftSignal"] >= 900)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "right"))
    },
    "both_bump_16": {
        "cumulant": (lambda observation:
                        int(observation["BumpRightSignal"] and
                            observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 1 - 1 / 16)
    },
    "both_bump_4": {
        "cumulant": (lambda observation:
                        int(observation["BumpRightSignal"] and
                            observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4)
    },
    "both_bump_action_backward_1": {
        "cumulant": (lambda observation:
                        int(observation["BumpRightSignal"] and
                            observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "backward"))
    },
    "both_bump_action_backward_4": {
        "cumulant": (lambda observation:
                        int(observation["BumpRightSignal"] and
                            observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4),
        "policy": (lambda observation, action: int(action == "backward"))
    },
    "both_bump_action_forward_1": {
        "cumulant": (lambda observation:
                        int(observation["BumpRightSignal"] and
                            observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "forward"))
    },
    "both_bump_action_forward_4": {
        "cumulant": (lambda observation:
                        int(observation["BumpRightSignal"] and
                            observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4),
        "policy": (lambda observation, action: int(action == "forward"))
    },
    "both_bump_action_left_1": {
        "cumulant": (lambda observation:
                        int(observation["BumpRightSignal"] and
                            observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "left"))
    },
    "both_bump_action_left_4": {
        "cumulant": (lambda observation:
                        int(observation["BumpRightSignal"] and
                            observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4),
        "policy": (lambda observation, action: int(action == "left"))
    },
    "both_bump_action_right_1": {
        "cumulant": (lambda observation:
                        int(observation["BumpRightSignal"] and
                            observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "right"))
    },
    "both_bump_action_right_4": {
        "cumulant": (lambda observation:
                        int(observation["BumpRightSignal"] and
                            observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4),
        "policy": (lambda observation, action: int(action == "right"))
    },
    "both_wheel_drop_16": {
        "cumulant": (lambda observation:
                        int(observation["WheeldropRight"] and
                            observation["WheeldropLeft"])),
        "gamma": (lambda observation: 1 - 1 / 16)
    },
    "both_wheel_drop_4": {
        "cumulant": (lambda observation:
                        int(observation["WheeldropRight"] and
                            observation["WheeldropLeft"])),
        "gamma": (lambda observation: 1 - 1 / 4)
    },
    # "both_wheel_overcurrent_4": {
    #     "cumulant": (lambda observation:
    #                     int(observation["RightWheelOvercurrent"] and
    #                         observation["LeftWheelOvercurrent"])),
    #     "gamma": (lambda observation: 1 - 1 / 4)
    # },
    "cliff_front_left_action_backward_1": {
        "cumulant": (lambda observation: int(
                        observation["CliffFrontLeftSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "backward"))
    },
    "cliff_front_left_action_forward_1": {
        "cumulant": (lambda observation: int(
                        observation["CliffFrontLeftSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "forward"))
    },
    "cliff_front_left_action_left_1": {
        "cumulant": (lambda observation: int(
                        observation["CliffFrontLeftSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "left"))
    },
    "cliff_front_left_action_right_1": {
        "cumulant": (lambda observation: int(
                        observation["CliffFrontLeftSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "right"))
    },
    "cliff_front_right_action_backward_1": {
        "cumulant": (lambda observation: int(
                        observation["CliffFrontRightSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "backward"))
    },
    "cliff_front_right_action_forward_1": {
        "cumulant": (lambda observation: int(
                        observation["CliffFrontRightSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "forward"))
    },
    "cliff_front_right_action_left_1": {
        "cumulant": (lambda observation: int(
                        observation["CliffFrontRightSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "left"))
    },
    "cliff_front_right_action_right_1": {
        "cumulant": (lambda observation: int(
                        observation["CliffFrontRightSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "right"))
    },
    "cliff_left_action_backward_1": {
        "cumulant": (lambda observation:
                        int(observation["CliffLeftSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "backward"))
    },
    "cliff_left_action_forward_1": {
        "cumulant": (lambda observation:
                        int(observation["CliffLeftSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "forward"))
    },
    "cliff_left_action_left_1": {
        "cumulant": (lambda observation:
                        int(observation["CliffLeftSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "left"))
    },
    "cliff_left_action_right_1": {
        "cumulant": (lambda observation:
                        int(observation["CliffLeftSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "right"))
    },
    "cliff_right_action_backward_1": {
        "cumulant": (lambda observation:
                        int(observation["CliffRightSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "backward"))
    },
    "cliff_right_action_forward_1": {
        "cumulant": (lambda observation:
                        int(observation["CliffRightSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "forward"))
    },
    "cliff_right_action_left_1": {
        "cumulant": (lambda observation:
                        int(observation["CliffRightSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "left"))
    },
    "cliff_right_action_right_1": {
        "cumulant": (lambda observation:
                        int(observation["CliffRightSignal"] >= 1000)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "right"))
    },
    "distance_negative_action_backward_4": {
        "cumulant": (lambda observation: int(observation["Distance"] < 0)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "backward"))
    },
    "distance_negative_action_forward_4": {
        "cumulant": (lambda observation: int(observation["Distance"] < 0)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "forward"))
    },
    "distance_negative_action_left_4": {
        "cumulant": (lambda observation: int(observation["Distance"] < 0)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "left"))
    },
    "distance_negative_action_right_4": {
        "cumulant": (lambda observation: int(observation["Distance"] < 0)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "right"))
    },
    "distance_positive_action_backward_4": {
        "cumulant": (lambda observation: int(observation["Distance"] > 0)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "backward"))
    },
    "distance_positive_action_forward_4": {
        "cumulant": (lambda observation: int(observation["Distance"] > 0)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "forward"))
    },
    "distance_positive_action_left_4": {
        "cumulant": (lambda observation: int(observation["Distance"] > 0)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "left"))
    },
    "distance_positive_action_right_4": {
        "cumulant": (lambda observation: int(observation["Distance"] > 0)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "right"))
    },
    "distance_zero_action_backward_4": {
        "cumulant": (lambda observation: int(observation["Distance"] == 0)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "backward"))
    },
    "distance_zero_action_forward_4": {
        "cumulant": (lambda observation: int(observation["Distance"] == 0)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "forward"))
    },
    "distance_zero_action_left_4": {
        "cumulant": (lambda observation: int(observation["Distance"] == 0)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "left"))
    },
    "distance_zero_action_right_4": {
        "cumulant": (lambda observation: int(observation["Distance"] == 0)),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "right"))
    },
    "force_field_16": {
        "cumulant": (lambda observation: int(observation["ForceField"])),
        "gamma": (lambda observation: 1 - 1 / 16)
    },
    "force_field_4": {
        "cumulant": (lambda observation: int(observation["ForceField"])),
        "gamma": (lambda observation: 1 - 1 / 4)
    },
    "green_buoy_16": {
        "cumulant": (lambda observation: int(observation["GreenBuoy"])),
        "gamma": (lambda observation: 1 - 1 / 16)
    },
    "green_buoy_4": {
        "cumulant": (lambda observation: int(observation["GreenBuoy"])),
        "gamma": (lambda observation: 1 - 1 / 4)
    },
    "green_buoy_force_field_16": {
        "cumulant": (lambda observation: int(observation["GreenBuoy"] and
                                             observation["ForceField"])),
        "gamma": (lambda observation: 1 - 1 / 16)
    },
    "green_buoy_force_field_4": {
        "cumulant": (lambda observation: int(observation["GreenBuoy"] and
                                             observation["ForceField"])),
        "gamma": (lambda observation: 1 - 1 / 4)
    },
    "left_bump_16": {
        "cumulant": (lambda observation: int(observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 1 - 1 / 16)
    },
    "left_bump_4": {
        "cumulant": (lambda observation: int(observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4)
    },
    "left_bump_action_backward_1": {
        "cumulant": (lambda observation: int(observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "backward"))
    },
    "left_bump_action_backward_4": {
        "cumulant": (lambda observation: int(observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4),
        "policy": (lambda observation, action: int(action == "backward"))
    },
    "left_bump_action_forward_1": {
        "cumulant": (lambda observation: int(observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "forward"))
    },
    "left_bump_action_forward_4": {
        "cumulant": (lambda observation: int(observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4),
        "policy": (lambda observation, action: int(action == "forward"))
    },
    "left_bump_action_left_1": {
        "cumulant": (lambda observation: int(observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "left"))
    },
    "left_bump_action_left_4": {
        "cumulant": (lambda observation: int(observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4),
        "policy": (lambda observation, action: int(action == "left"))
    },
    "left_bump_action_right_1": {
        "cumulant": (lambda observation: int(observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "right"))
    },
    "left_bump_action_right_4": {
        "cumulant": (lambda observation: int(observation["BumpLeftSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4),
        "policy": (lambda observation, action: int(action == "right"))
    },
    "left_wheel_drop_16": {
        "cumulant": (lambda observation: int(observation["WheeldropLeft"])),
        "gamma": (lambda observation: 1 - 1 / 16)
    },
    "left_wheel_drop_4": {
        "cumulant": (lambda observation: int(observation["WheeldropLeft"])),
        "gamma": (lambda observation: 1 - 1 / 4)
    },
    # "left_wheel_overcurrent_4": {
    #     "cumulant": (lambda observation:
    #                     int(observation["LeftWheelOvercurrent"])),
    #     "gamma": (lambda observation: 1 - 1 / 4)
    # },
    "red_buoy_16": {
        "cumulant": (lambda observation: int(observation["RedBuoy"])),
        "gamma": (lambda observation: 1 - 1 / 16)
    },
    "red_buoy_4": {
        "cumulant": (lambda observation: int(observation["RedBuoy"])),
        "gamma": (lambda observation: 1 - 1 / 4)
    },
    "red_buoy_force_field_16": {
        "cumulant": (lambda observation: int(observation["RedBuoy"] and
                                             observation["ForceField"])),
        "gamma": (lambda observation: 1 - 1 / 16)
    },
    "red_buoy_force_field_4": {
        "cumulant": (lambda observation: int(observation["RedBuoy"] and
                                             observation["ForceField"])),
        "gamma": (lambda observation: 1 - 1 / 4)
    },
    "red_buoy_green_buoy_force_field_16": {
        "cumulant": (lambda observation: int(observation["RedBuoy"] and
                                             observation["GreenBuoy"] and
                                             observation["ForceField"])),
        "gamma": (lambda observation: 1 - 1 / 16)
    },
    "red_buoy_green_buoy_force_field_4": {
        "cumulant": (lambda observation: int(observation["RedBuoy"] and
                                             observation["GreenBuoy"] and
                                             observation["ForceField"])),
        "gamma": (lambda observation: 1 - 1 / 4)
    },
    "reserved_16": {
        "cumulant": (lambda observation: int(observation["Reserved"])),
        "gamma": (lambda observation: 1 - 1 / 16)
    },
    "reserved_4": {
        "cumulant": (lambda observation: int(observation["Reserved"])),
        "gamma": (lambda observation: 1 - 1 / 4)
    },
    "right_bump_16": {
        "cumulant": (lambda observation: int(observation["BumpRightSignal"])),
        "gamma": (lambda observation: 1 - 1 / 16)
    },
    "right_bump_4": {
        "cumulant": (lambda observation: int(observation["BumpRightSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4)
    },
    "right_bumpaction__backward_1": {
        "cumulant": (lambda observation: int(observation["BumpRightSignal"])),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "backward"))
    },
    "right_bumpaction__backward_4": {
        "cumulant": (lambda observation: int(observation["BumpRightSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4),
        "policy": (lambda observation, action: int(action == "backward"))
    },
    "right_bumpaction__forward_1": {
        "cumulant": (lambda observation: int(observation["BumpRightSignal"])),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "forward"))
    },
    "right_bumpaction__forward_4": {
        "cumulant": (lambda observation: int(observation["BumpRightSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4),
        "policy": (lambda observation, action: int(action == "forward"))
    },
    "right_bumpaction__left_1": {
        "cumulant": (lambda observation: int(observation["BumpRightSignal"])),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "left"))
    },
    "right_bumpaction__left_4": {
        "cumulant": (lambda observation: int(observation["BumpRightSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4),
        "policy": (lambda observation, action: int(action == "left"))
    },
    "right_bumpaction__right_1": {
        "cumulant": (lambda observation: int(observation["BumpRightSignal"])),
        "gamma": (lambda observation: 0),
        "policy": (lambda observation, action: int(action == "right"))
    },
    "right_bumpaction__right_4": {
        "cumulant": (lambda observation: int(observation["BumpRightSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4),
        "policy": (lambda observation, action: int(action == "right"))
    },
    "right_wheel_drop_16": {
        "cumulant": (lambda observation: int(observation["WheeldropRight"])),
        "gamma": (lambda observation: 1 - 1 / 16)
    },
    "right_wheel_drop_4": {
        "cumulant": (lambda observation: int(observation["WheeldropRight"])),
        "gamma": (lambda observation: 1 - 1 / 4)
    },
    # "right_wheel_overcurrent_4": {
    #     "cumulant": (lambda observation:
    #                     int(observation["RightWheelOvercurrent"])),
    #     "gamma": (lambda observation: 1 - 1 / 4)
    # },
    "wall_1": {
        "cumulant": (lambda observation: int(observation["WallSignal"])),
        "gamma": (lambda observation: 0)
    },
    "wall_16": {
        "cumulant": (lambda observation: int(observation["WallSignal"])),
        "gamma": (lambda observation: 1 - 1 / 16)
    },
    "wall_4": {
        "cumulant": (lambda observation: int(observation["WallSignal"])),
        "gamma": (lambda observation: 1 - 1 / 4)
    }
}
