
import math
import numpy as np

from .evaluator.evaluator import Evaluator
from .GTD.BinaryGTD import BinaryGTD
from .gvfs import gvfs
from .tile_coding.tile_coder_adapter import TileCoderAdapter
from .TOTD.BinaryTOTD import BinaryTOTD
from collections import namedtuple


GVF_ON = namedtuple("GVF_ON", ["cumulant", "gamma"])
GVF_OFF = namedtuple("GVF_OFF", GVF_ON._fields + ("policy",))


class GVFLS:

    def __init__(self,
                 action,
                 observation,
                 action_probabilities,
                 batch,
                 clobber,
                 run_id):
        # Setup representation
        self.tile_coder = TileCoderAdapter(
                                    sensor_groups=[[0, 1, 2, 3]],
                                    tiling_groups=[[(1, 20, 8),
                                                    (2, 20, 8),
                                                    (3, 20, 8),
                                                    (4, 20, 8)]],
                                    sensor_ranges=[(0.0, 2000.0),
                                                   (0.0, 2000.0),
                                                   (0.0, 2000.0),
                                                   (0.0, 2000.0)],
                                    memory_size=4096,
                                    bias=True,
                                    fine_grouping=False,
                                    tiling_features=["CliffLeftSignal",
                                                     "CliffFrontLeftSignal",
                                                     "CliffFrontRightSignal",
                                                     "CliffRightSignal"])
        theta_size = self.tile_coder.theta_size
        num_tilings = self.tile_coder.num_tilings
        phi = self.tile_coder.get_tiling(observation)

        # Separate gvfs into onpolicy and offpolicy
        self.onpolicy_gvfs = dict()
        self.offpolicy_gvfs = dict()
        for name, parameters in sorted(gvfs.items()):
            if "policy" in parameters:  # offpolicy
                self.offpolicy_gvfs[name] = GVF_OFF(parameters["cumulant"],
                                                    parameters["gamma"],
                                                    parameters["policy"])
            else:  # onpolicy
                self.onpolicy_gvfs[name] = GVF_ON(parameters["cumulant"],
                                                  parameters["gamma"])

        # Setup onpolicy learner
        onpolicy_alpha = np.ones(len(self.onpolicy_gvfs)) * 0.1 / num_tilings
        onpolicy_lambda = np.ones(len(self.onpolicy_gvfs)) * 0.9
        self.onpolicy_gamma = np.ones(len(self.onpolicy_gvfs))
        onpolicy_theta = np.zeros((len(self.onpolicy_gvfs), theta_size))
        for i, (_, gvf) in enumerate(sorted(self.onpolicy_gvfs.items())):
            self.onpolicy_gamma[i] = gvf.gamma(observation)
        self.onpolicy_learner = BinaryTOTD(onpolicy_alpha,
                                           onpolicy_lambda,
                                           self.onpolicy_gamma,
                                           onpolicy_theta,
                                           phi)
        self.onpolicy_cumulant = np.zeros(len(self.onpolicy_gvfs))

        # Setup onpolicy evaluator
        self.onpolicy_evaluator = Evaluator(len(self.onpolicy_gvfs),
                                            batch=batch,
                                            run=run_id,
                                            clobber=clobber)
        self.onpolicy_rho = np.ones(len(self.onpolicy_gvfs))
        self.onpolicy_V = np.zeros(len(self.onpolicy_gvfs))
        self.onpolicy_V_prime = np.zeros(len(self.onpolicy_gvfs))

        # Setup offpolicy learner
        offpolicy_alpha = np.ones(len(self.offpolicy_gvfs)) * 0.1 / num_tilings
        off_policy_beta = np.ones(len(self.offpolicy_gvfs)) * 0.01
        offpolicy_lambda = np.ones(len(self.offpolicy_gvfs)) * 0.9
        self.offpolicy_gamma = np.ones(len(self.offpolicy_gvfs))
        offpolicy_theta = np.zeros((len(self.offpolicy_gvfs), theta_size))
        for i, (_, gvf) in enumerate(sorted(self.offpolicy_gvfs.items())):
            self.offpolicy_gamma[i] = gvf.gamma(observation)
        self.offpolicy_learner = BinaryGTD(offpolicy_alpha,
                                           off_policy_beta,
                                           offpolicy_lambda,
                                           self.offpolicy_gamma,
                                           offpolicy_theta,
                                           phi)
        self.offpolicy_cumulant = np.zeros(len(self.offpolicy_gvfs))
        self.offpolicy_rho = np.zeros(len(self.offpolicy_gvfs))
        for i, (_, gvf) in enumerate(sorted(self.offpolicy_gvfs.items())):
            self.offpolicy_rho[i] = (gvf.policy(observation, action) /
                                     action_probabilities[action])

        # Setup offpolicy evaluator
        self.offpolicy_evaluator = Evaluator(len(self.offpolicy_gvfs),
                                             batch=batch,
                                             run=run_id,
                                             clobber=clobber)
        self.offpolicy_V = np.zeros(len(self.offpolicy_gvfs))
        self.offpolicy_V_prime = np.zeros(len(self.offpolicy_gvfs))


        # Set init variables
        self.last_observation = observation
        self.last_action = action
        self.last_phi = phi
        self.last_offpolicy_rho = self.offpolicy_rho.copy()

    def update(self, action, observation, action_probabilities):
        phi = self.tile_coder.get_tiling(observation)

        # Add predictions to observation
        observation.update(self.predict(self.last_observation,
                                        phi=self.last_phi))

        # Update cumulant, gamma, rho, and V_prime
        for i, (name, gvf) in enumerate(sorted(self.onpolicy_gvfs.items())):
            self.onpolicy_cumulant[i] = gvf.cumulant(observation)
            self.onpolicy_gamma[i] = gvf.gamma(observation)
            self.onpolicy_V_prime[i] = observation[name]
        for i, (name, gvf) in enumerate(sorted(self.offpolicy_gvfs.items())):
            self.offpolicy_cumulant[i] = gvf.cumulant(observation)
            self.offpolicy_gamma[i] = gvf.gamma(observation)
            self.offpolicy_rho[i] = (gvf.policy(observation, action) /
                                     action_probabilities[action])
            self.offpolicy_V_prime[i] = observation[name]

        # Update learners
        self.onpolicy_learner.update(phi,
                                     self.onpolicy_cumulant,
                                     gamma=self.onpolicy_gamma)
        self.offpolicy_learner.update(phi,
                                      self.offpolicy_cumulant,
                                      self.last_offpolicy_rho,
                                      gamma=self.offpolicy_gamma)

        # Update evaluators
        self.onpolicy_evaluator.update(self.onpolicy_cumulant,
                                       self.onpolicy_gamma,
                                       self.onpolicy_rho,
                                       self.onpolicy_V,
                                       self.onpolicy_V_prime)
        self.offpolicy_evaluator.update(self.offpolicy_cumulant,
                                        self.offpolicy_gamma,
                                        self.offpolicy_rho,
                                        self.offpolicy_V,
                                        self.offpolicy_V_prime)

        # Save action and observation
        self.last_observation = observation
        self.last_action = action
        self.last_phi = phi
        (self.last_offpolicy_rho, self.offpolicy_rho) = (self.offpolicy_rho,
                                                         self.last_offpolicy_rho)
        (self.onpolicy_V, self.onpolicy_V_prime) = (self.onpolicy_V_prime,
                                                    self.onpolicy_V)
        (self.offpolicy_V, self.offpolicy_V_prime) = (self.offpolicy_V_prime,
                                                      self.offpolicy_V)

    def predict(self, observation, phi=None):
        if phi is None:
            phi = self.tile_coder.get_tiling(observation)

        rv = dict()

        # Get onpolicy predictions
        onpolicy_predictions = self.onpolicy_learner.predict(phi)
        for i, name in enumerate(sorted(self.onpolicy_gvfs.keys())):
            rv[name] = onpolicy_predictions[i]

        # Get offpolicy predictions
        offpolicy_predictions = self.offpolicy_learner.predict(phi)
        for i, name in enumerate(sorted(self.offpolicy_gvfs.keys())):
            rv[name] = offpolicy_predictions[i]

        return rv

    def compute_score(self):
        rv = dict()

        # Get onpolicy scores
        onpolicy_scores = self.onpolicy_evaluator.compute_score()
        for i, (name, _) in enumerate(sorted(self.onpolicy_gvfs.items())):
            rv[name] = onpolicy_scores[i]

        # Get offpolicy scores
        offpolicy_scores = self.offpolicy_evaluator.compute_score()
        for i, (name, _) in enumerate(sorted(self.offpolicy_gvfs.items())):
            rv[name] = offpolicy_scores[i]

        return rv

    def dumps_onpolicy_learner_theta(self):
        return json.dumps(self.onpolicy_learner.theta.tolist())

    def dumps_offpolicy_learner_theta(self):
        return json.dumps(self.offpolicy_learner.theta.tolist())

    def loads_onpolicy_learner_theta(self, json_):
        self.onpolicy_learner.theta = np.asarray(json.loads(json_))

    def loads_offpolicy_learner_theta(self, json_):
        self.offpolicy_learner.theta = np.asarray(json.loads(json_))

    def dump_onpolicy_learner_theta(self, filename):
        with open(filename, 'w') as outfile:
            outfile.write(self.dumps_onpolicy_learner_theta())

    def dump_offpolicy_learner_theta(self, filename):
        with open(filename, 'w') as outfile:
            outfile.write(self.dumps_offpolicy_learner_theta())

    def load_onpolicy_learner_theta(self, filename):
        with open(filename, 'r') as infile:
            self.loads_onpolicy_learner_theta(infile.read().strip())

    def load_offpolicy_learner_theta(self, filename):
        with open(filename, 'r') as infile:
            self.loads_offpolicy_learner_theta(infile.read().strip())
