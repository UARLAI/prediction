
import logging
import select
import time
import warnings

from cspy3 import Controller, Parser
from logs.csv_logger import CSVLogger
from reflexes import Reflexes

# Disable debug logging
logging.disable(logging.DEBUG)

WARN_THRESHOLD = 10 / 1000  # 10 milliseconds


class StreamManager:
    """Reads streaming data using serial, parses the data into a
    readable form, and calls a handler on the parsed data before adding
    it to the output stream.
    """
    def __init__(self,
                 stream,
                 parser,
                 handler,
                 output,
                 batch=None,
                 clobber=False):
        self.batch = batch
        self.clobber = True if self.batch == "default" else clobber
        self.handler = handler
        self.output = output
        self.parser = parser
        self.stream = stream

    def run(self, halt_flag):
        """Begin the stream input processing loop."""
        observation_id = 0
        first_run = True

        while not halt_flag.is_set():
            ready, _, _ = select.select([self.stream], [], [], 0)

            if ready:
                start_time = time.time()

                # Read and parse the next piece of data
                data = self.stream.read()
                self.parser(* data)

                # If valid then handle data and move to output
                while self.parser.output:
                    observation_id += 1
                    observation_time = time.time()
                    observation = self.parser.output.pop()
                    observation.update({"observation_id": observation_id,
                                        "observation_time": observation_time})
                    self.handler.run(observation)

                    # Log observation
                    if self.batch is not None:

                        if first_run:
                            header = [item[0] for item in sorted(
                                     observation.items())]
                            log = CSVLogger(self.batch,
                                            "stream_observations.csv",
                                            self.clobber,
                                            header=header)
                            first_run = False
                        log.write([str(item[1]) for item in sorted(
                                   observation.items())])

                    self.output.put(observation)

                # Warn the user if too much time has passed
                if time.time() - start_time > WARN_THRESHOLD:
                    warnings.warn(
                        "stream data handling time exceeds threshold")


def start_stream_manager(flag,
                         port,
                         control_lock,
                         sensors,
                         output,
                         batch=None,
                         clobber=False):
    """Creates and starts a new StreamManager on the given port."""
    try:
        with control_lock:
            control = Controller(port)
            control.mode_full()
            control.request_stream(* sensors)
        StreamManager(control.ser,
                      Parser(* sensors),
                      Reflexes(control, control_lock),
                      output,
                      batch=batch,
                      clobber=clobber).run(flag)
    finally:
        flag.set()
