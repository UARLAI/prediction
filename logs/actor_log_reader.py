
from collections import namedtuple
from tools import destringify_number


class ActorLogReader():

    def __init__(self, run_log_reader, stream_log_path, sense_log_path=None):
        """
        Read an actor log along with the raw observations file(s) it references.

        The actor log must be in the form of one or more CSVs, with each CSV 
        corresponding to a run (under the current architecture, there should 
        never be more than one run). See csv_run_log_reader.py for the CSV 
        specifications.

        Args:
            run_log_reader (CSVRunLogReader): A CSVRunLogReader that reads from 
                the actor log CSVs.
            stream_log_path (str): The path of stream_observations.csv 
                (including stream_observations.csv itself).
            sense_log_path (str): The path of sense_observations.csv (including 
                sense_observations.csv itself).
        """
        self.run_log_reader = run_log_reader
        self.stream_log = open(stream_log_path, 'r')
        self.StreamEntry = \
                namedtuple('StreamEntry',
                           self.stream_log.readline().strip('\n').split(','))
        if sense_log_path != None:
            self.sense_log = open(sense_log_path, 'r')
            self.SenseEntry = \
                    namedtuple('SenseEntry',
                               self.sense_log.readline().strip('\n').split(','))
        else:
            self.sense_log = None
        self.obs_action_buffer = []
        self.obs_ptr = 0
        self.action_ptr = 0

    def _next_run_entry(self):
        # Get the next row of the run log
        self.run_entry = self.run_log_reader.get_entry()

        # Get all the stream observations from m+1 to n, where m is the 
        # observation ID of the previous row of the run log and n is the 
        # observation ID of the current row of the run log
        stream_entry = self.StreamEntry(*[destringify_number(_)
                                          for _ in self.stream_log.readline().strip('\n').split(',')])
        stream_entries = [stream_entry._asdict()]
        while self.run_entry.LastStreamObservationID != stream_entry.observation_id:
            stream_entry = self.StreamEntry(*[destringify_number(_)
                                              for _ in self.stream_log.readline().strip('\n').split(',')])
            stream_entries.append(stream_entry._asdict())

        # Do the same for the sense observations, if applicable
        if self.sense_log != None:

            sense_entry = self.SenseEntry(*[destringify_number(_)
                                            for _ in self.sense_log.readline().strip('\n').split(',')])
            sense_entries = [sense_entry._asdict()]
            while self.run_entry.LastSenseObservationID != sense_entry.observation_id:
                sense_entry = self.SenseEntry(*[destringify_number(_)
                                                for _ in self.sense_log.readline().strip('\n').split(',')])
                sense_entries.append(sense_entry._asdict())

            obs_entries = (stream_entries, sense_entries)

        else:

            obs_entries = (stream_entries,)

        # Add the observations gathered above plus the action associated with 
        # the current row of the run log as an observations-action pair to the 
        # buffer
        self.obs_action_buffer.append((obs_entries, self.run_entry.Action))

    def _end_of_run(self):
        return self.run_log_reader.end_of_run

    def open_run(self):
        """
        Open the next run; that is, open the next actor log CSV.

        Returns:
            False if there are no more CSVs, True otherwise.
        """
        result = self.run_log_reader.open_run()
        # Close the observation log when there are no more run logs
        if result == False:
            self.stream_log.close()
            if self.sense_log != None:
                self.sense_log.close()
        return result

    def start_iteration(self):
        """
        Indicate that the next actor iteration has started.
        """
        if len(self.obs_action_buffer) > 0:
            self.obs_action_buffer.pop(0)
        self.obs_ptr = self.action_ptr = 0

    def get_observations(self):
        """
        Get the observations corresponding to an actor iteration.

        Returns:
            A list of stream observations with IDs in the range 
            (lastStreamObservationID_k-1, LastStreamObservationID_k], where
            lastStreamObservationID_i is the value of the 
            lastStreamObservationID field for the row of the actor log whose
            Number (i.e. iteration) field has value i, if such stream 
            observations are available. k is calculated as 
            follows:
                k = number of times start_iteration has been called 
                    + number of times get_observations has been called since 
                      the last start_iteration call (not including the current
                      get_observations call)
            Each stream observation in the list is a named tuple whose keys 
            correspond to the stream_observations.csv header.
            
            If such stream observatiosn are not available, this method returns 
            None.

            Note: In calculating k, when determining the number of times 
            start_iteration was called, a call should only be counted if 
            get_observations or get_action was called at least once afterward 
            and before any subsequent start_iteration call.
        """
        if self.obs_ptr == len(self.obs_action_buffer):
            if self._end_of_run():
                return None
            self._next_run_entry()
        self.obs_ptr += 1
        return self.obs_action_buffer[self.obs_ptr - 1][0]

    def get_action(self):
        """
        Get the action corresponding to an actor iteration.

        Returns:
            The action corresponding to the row of the actor log whose Number 
            (i.e. iteration) field has value k, if such an action is available. 
            k is calculated as follows:
                k = number of times start_iteration has been called 
                    + number of times get_action has been called since 
                      the last start_iteration call (not including the current
                      get_action call)

            If such an action is not available, this method returns None.

            Note: In calculating k, when determining the number of times 
            start_iteration was called, a call should only be counted if 
            get_observations or get_action was called at least once afterward 
            and before any subsequent start_iteration call.
        """
        if self.action_ptr == len(self.obs_action_buffer):
            if self._end_of_run():
                return None
            self._next_run_entry()
        self.action_ptr += 1
        return self.obs_action_buffer[self.action_ptr - 1][1]

    def end_of_run(self):
        """
        If there are no more rows in the run log and zero or one observations-
        action pair(s) remain(s) in the buffer, the end of the run has been 
        reached (a single pair left in the buffer would just get deleted if 
        start_iteration() were to be called on a new iteration).

        Return:
            True if the end of the run has been reached, False otherwise.
        """
        return self._end_of_run() and len(self.obs_action_buffer) <= 1
