
import os


class CSVLogger:
    """
    Log data to a CSV.

    Args:
        batch (str): The name of the directory to save the log to
        name (str): The name of the log
        clobber (bool): If True, overwrite any existing log with the same name 
            at the same location
        header (list): Log header fields (a value of None results in no header)
        number (bool): If True, add a Number (i.e. iteration) field to the log 
    """

    def __init__(self, batch, name, clobber=False, header=None, number=False):

        # Make log directory if it does not exist yet
        if not os.path.exists(batch):
            os.makedirs(batch)

        # Make log name
        if name[- 4:] != ".csv":
            name = "{}.csv".format(name)
        self.log = os.path.join(batch, name)

        # Delete the file if clobber is on
        if clobber and os.path.exists(self.log):
            os.remove(self.log)

        # Setup numbering system
        if number:
            if os.path.exists(self.log):
                with open(self.log, 'r') as infile:
                    self.number = [int(line.split(",")[0])
                                   for line in infile][- 1]
            else:
                self.number = 1
        else:
            self.number = None

        # Write header if one was given
        if header and not os.path.exists(self.log):
            if self.number is not None:
                self._write_header(["Number"] + header)
            else:
                self._write_header(header)

    def _write_header(self, header):
        with open(self.log, "a") as outfile:
            outfile.write(
                ("{}," * (len(header) - 1) + "{}\n").format(* header))

    def write(self, data):
        """
        Add data as a new entry in the CSV.

        Args:
            data (list): The data to add, with the list order determined by the 
                CSV header.
        """
        if self.number is not None:
            data = [self.number] + data
            self.number += 1

        with open(self.log, "a") as outfile:
            outfile.write(("{}," * (len(data) - 1) + "{}\n").format(* data))
