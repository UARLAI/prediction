import os
from collections import namedtuple
from tools import destringify_number

class CSVRunLogReader:
    """
    Read a collection of CSVs.

    Each CSV's name must be of the form [name]_run_[k].csv, where [name] is the 
    same for all the CSVs and [k] is a number between 1 and N inclusive, where 
    N is the number of CSVs (X has to be unique for each CSV). Furthermore, 
    each CSV's top row needs to be a header row. Each CSV's header can, but 
    need not, be unique.

    Args:
        log_dir (str): The directory containing the CSVs to read
        log_name (str): The name of the CSVs (ignoring the _run_[k].csv 
            ending). For example, "actor_log" would resolve to CSVs with names 
            of the form actor_log_run_[k].csv.
    """

    def __init__(self, log_dir, log_name):
        self.log_dir = log_dir
        self.log_name = log_name
        self.run_count = len([_ for _ in os.listdir(log_dir) 
                              if os.path.isfile(os.path.join(log_dir, _)) 
                                 and self.log_name + '_run' in _])
        self.run = 0
        self.end_of_run = False

    def _read_entry(self):
        line = self.current_log.readline().strip('\n')
        self.next_entry = self.Entry(*[destringify_number(_) for _ in line.split(',')]) if line and line != '' else None

        # No more entries; reached end of the log
        if self.next_entry == None:
            self.current_log.close()
            self.end_of_run = True
            self.run += 1

    def open_run(self):
        """
        Open the next run; that is, open the next CSV in the collection.

        Returns:
            False if there is no CSV to open (because all the CSVs have already 
                been read), True otherwise.
        """
        # Abort if there are no more runs
        if self.run == self.run_count:
            return False

        # Open the log file
        self.current_log = open(os.path.join(self.log_dir, 
                                             self.log_name + \
                                             '_run_{}.csv'.format(self.run)), 'r')

        self.end_of_run = False

        # Define a namedtuple based on the log's fields
        # Since this is done each time a new run is opened, the log for each 
        # run can have a different header.
        self.Entry = namedtuple('Entry', 
                                self.current_log.readline().strip('\n').split(','))

        # Get the first entry
        self._read_entry()

        return True
        
    def get_entry(self):
        """
        Get the next line in the open CSV.

        Returns
            A named tuple whose keys correspond to the CSV header.
        """
        current_entry = self.next_entry
        # Read the entry after the one being requested
        self._read_entry()
        return current_entry
