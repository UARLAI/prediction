
from abc import ABC, abstractmethod


class Actor(ABC):
    """Acts on data that has been processed."""
    def __init__(self, batch, clobber):
        self.batch = batch
        self.clobber = True if self.batch == "default" else clobber

    def get_choices(self, observation):
        choices = ["left", "right"]
        if not (observation["BumpRightSignal"] and
                observation["BumpLeftSignal"]):
            choices.append("forward")
        if not (observation["WheeldropRight"] and
                observation["WheeldropLeft"]):
            choices.append("backward")
        return choices

    @abstractmethod
    def get_next_observation(self):
        pass

    @abstractmethod
    def run(self):
        pass

    @abstractmethod
    def single_run(self):
        pass
