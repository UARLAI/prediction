
import math
import numpy as np
import random
import time
import warnings

from .actor import Actor
from .parse_observation import parse_observations
from gvfls.gvfls import GVFLS
from logs.csv_logger import CSVLogger
from remote_exporter.remote_exporter import RemoteExporter


class OnlineActor(Actor):
    """Acts on data that has been processed."""
    def __init__(self,
                 args,
                 control=None,
                 control_lock=None,
                 stream_output=None,
                 sense_output=None,
                 camera_output=None):
        super().__init__(args["batch"], args["clobber"])

        # Passed in single arguments
        self.camera_output = camera_output
        self.control = control
        self.control_lock = control_lock
        self.sense_output = sense_output
        self.stream_output = stream_output

        # Arguments contained in args
        self.beep = args["beep"]
        self.loop_interval = args["loop_interval"]
        self.period = args["period"]
        self.runs = args["runs"]
        self.speed = args["speed"]

        # Other init stuff
        max_distance = self.speed * self.loop_interval
        max_angle = 180 * max_distance / (240 * math.pi)
        self.angle_range = (-1.1 * max_angle, 1.1 * max_angle)
        self.camera_obs = None
        self.dist_range = (-1.1 * max_distance, 1.1 * max_distance)
        self.mode = "automatic"
        self.mode_button_pressed = False
        self.remote_exporter = RemoteExporter(args["export_protocol"])
        self.sense_obs = None

    def take_action(self, action):
        """Translates logical action to create action."""
        with self.control_lock:
            if action is None or action == "stop":
                    self.control.drive_direct(0, 0)
            elif action == "left":
                    self.control.drive_direct(self.speed, - self.speed)
            elif action == "right":
                    self.control.drive_direct(- self.speed, self.speed)
            elif action == "forward":
                    self.control.drive_direct(self.speed, self.speed)
            elif action == "backward":
                    self.control.drive_direct(- self.speed, - self.speed)
            else:
                raise ValueError("illegal action")

    def get_next_observation(self, sleep_time):
        """Read the next available observation from the Create."""
        time.sleep(sleep_time)

        # Wait for stream output to become available
        while not self.halt_flag.is_set():
                if not self.stream_output.empty():
                    stream_obs = list()
                    while not self.stream_output.empty():
                        stream_obs.append(self.stream_output.get())
                    break

        # Wait for sense hat output if available
        if self.sense_output is not None:
            while not self.halt_flag.is_set():
                if not self.sense_output.empty():
                    sense_obs = list()
                    while not self.sense_output.empty():
                        sense_obs.append(self.sense_output.get())
                    break
        else:
            sense_obs = None

        # Wait for camera output if available
        if self.camera_output is not None:
            while not self.halt_flag.is_set():
                if not self.camera_output.empty():
                    camera_obs = list()
                    while not self.camera_output.empty():
                        camera_obs.append(self.camera_output.get())
                    break
        else:
            camera_obs = None

        return parse_observations(stream_obs, sense_obs, camera_obs)

    def log_iteration(self, action, observation, filename, first_run=True):
        """Writes observation data to a file."""
        if first_run:
            # Write the header and make the csv
            header = ["Timestamp", "Action", "LastStreamObservationID"]
            if self.sense_output is not None:
                header.append("LastSenseObservationID")
            if self.camera_output is not None:
                header.append("LastCameraObservationID")
            self.run_log = CSVLogger(self.batch,
                                     filename,
                                     self.clobber,
                                     header=header,
                                     number=True)

        # Write the single observation data
        data = [time.time(), action, observation["StreamObservationID"][-1]]
        if self.sense_output is not None:
            data.append(observation["SenseObservationID"][-1])
        if self.camera_output is not None:
            data.append(observation["CameraObservationID"][-1])
        self.run_log.write(data)

    def run(self, halt_flag=None):
        """Do a series of consecutive runs."""
        try:
            self.halt_flag = halt_flag
            for i in range(self.runs):
                self.single_run(run_id=i)

            # Just try to set the flag and ignore it if it fails
            halt_flag.set()

        finally:
            # Shut down the robot
            self.take_action("stop")
            with self.control_lock:
                self.control.shutdown()

    def single_run(self, run_id=0):
        """Begin the action loop."""

        # Setup music
        with self.control_lock:
            for i in range(16):
                self.control.save_song(
                    i,
                    [(int(31 + i * (127 - 31) / 15), 32)])

        # Get the first observation (we should probably log this)
        observation = self.get_next_observation(0)

        # Enter loop
        iteration = 0
        start_time = iteration_start_time = time.time()
        while (not self.halt_flag.is_set() and
               time.time() - start_time < self.period):

            # Check for mode change
            if observation["IRByte"] == "Power":
                if self.mode_button_pressed is False:
                    self.mode = ("automatic" if self.mode == "manual"
                                 else "manual")
                    self.mode_button_pressed = True
                else:
                    self.mode_button_pressed = False

            # Take action
            if self.mode == "automatic":
                choices = self.get_choices(observation)
            elif self.mode == "manual":
                if observation["IRByte"] in ["Left", "Right", "Forward"]:
                    choices = [observation["IRByte"].lower()]
                elif observation["IRByte"] == "LargeClean":
                    choices = ["backward"]
                else:
                    choices = ["stop"]
            else:
                raise ValueError("illegal mode")
            action = random.choice(choices)
            self.take_action(action)

            # Log this iteration
            self.log_iteration(action,
                               observation,
                               "actor_log_run_{}.csv".format(run_id),
                               first_run=(iteration == 0))

            # Send the observation for visualization
            self.remote_exporter.send(observation)
            self.remote_exporter.increment_timestep()

            # Update GVFLS
            action_probability = {action: 1 / len(choices)
                                  for action in choices}
            if iteration == 0:
                # Setup GVFLS
                gvfls = GVFLS(action,
                              observation,
                              action_probability,
                              self.batch,
                              self.clobber,
                              run_id)
            gvfls.update(action, observation, action_probability)

            # Get next observation
            observation = self.get_next_observation(
                            max([self.loop_interval -
                                (time.time() - iteration_start_time), 0]))

            # Increment iteration counter
            iteration += 1

            # Warn the user if too much time has passed
            if time.time() - iteration_start_time > self.loop_interval:
                warnings.warn("actor data handling time exceeds loop interval")

            # Get next iteration start time
            iteration_start_time = time.time()
