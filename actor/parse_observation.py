
IRByte_Dictionary = {129: "Left",
                     130: "Forward",
                     131: "Right",
                     132: "Spot",
                     133: "Max",
                     134: "Small",
                     135: "Medium",
                     136: "LargeClean",
                     137: "Pause",
                     138: "Power",
                     139: "ArcForwardLeft",
                     140: "ArcForwardRight",
                     141: "DriveStop",
                     142: "SendAll",
                     143: "SeekDock",
                     240: "Reserved",
                     248: "RedBuoy",
                     244: "GreenBuoy",
                     242: "ForceField",
                     252: "RedBuoyGreenBuoy",
                     250: "RedBuoyForceField",
                     246: "GreenBuoyForceField",
                     254: "RedBuoyGreenBuoyForceField"}

def _concatenate_observations(raw_observations, parse_function):
    """Parses a series of observations and concatenate the resulting
    values together into a list.
    """
    observations = parse_function(raw_observations[0])

    # Make each value into a list
    for key, value in observations.items():
        observations[key] = [value]

    # Append all the other observations
    for raw_observation in raw_observations[1:]:
        for key, value in parse_function(raw_observation).items():
            observations[key].append(value)

    return observations


def parse_stream_observation(raw_observation):
    """Parses one observation from the create stream into a more
    meaningful form.
    """
    observation = dict()
    observation["StreamObservationID"] = (
        raw_observation["observation_id"])
    observation["StreamObservationTime"] = (
        raw_observation["observation_time"])
    observation["WheeldropCaster"] = (
        bool(raw_observation["BumpsAndWheelDrops"] & (1 << 4)))
    observation["WheeldropLeft"] = (
        bool(raw_observation["BumpsAndWheelDrops"] & (1 << 3)))
    observation["WheeldropRight"] = (
        bool(raw_observation["BumpsAndWheelDrops"] & (1 << 2)))
    observation["BumpLeftSignal"] = (
        bool(raw_observation["BumpsAndWheelDrops"] & (1 << 1)))
    observation["BumpRightSignal"] = (
        bool(raw_observation["BumpsAndWheelDrops"] & 1))
    observation["VirtualWall"] = (
        bool(raw_observation["VirtualWall"]))
    # observation["LeftWheelOvercurrent"] = (
    #     bool(raw_observation["Overcurrents"] & (1 << 4)))
    # observation["RightWheelOvercurrent"] = (
    #     bool(raw_observation["Overcurrents"] & (1 << 3)))
    observation["IRByte"] = (
        IRByte_Dictionary.get(raw_observation["IRByte"]))
    observation["RedBuoy"] = (
        bool(raw_observation["IRByte"] & 0b11111000 == 0b11111000))
    observation["GreenBuoy"] = (
        bool(raw_observation["IRByte"] & 0b11110100 == 0b11110100))
    observation["ForceField"] = (
        bool(raw_observation["IRByte"] & 0b11110010 == 0b11110010))
    observation["Reserved"] = (
        IRByte_Dictionary.get(raw_observation["IRByte"]) == "Reserved")
    observation["Distance"] = (
        raw_observation["Distance"])
    observation["Angle"] = (
        raw_observation["Angle"])
    observation["BatteryCharge"] = (
        raw_observation["BatteryCharge"])
    observation["WallSignal"] = (
        bool(raw_observation["WallSignal"]))
    observation["CliffLeftSignal"] = (
        raw_observation["CliffLeftSignal"])
    observation["CliffFrontLeftSignal"] = (
        raw_observation["CliffFrontLeftSignal"])
    observation["CliffFrontRightSignal"] = (
        raw_observation["CliffFrontRightSignal"])
    observation["CliffRightSignal"] = (
        raw_observation["CliffRightSignal"])
    observation["HomeChargingSourcesAvailable"] = (
        raw_observation["ChargingSourcesAvailable"] & (1 << 1))
    observation["InternalChargingSourcesAvailable"] = (
        raw_observation["ChargingSourcesAvailable"] & 1)
    return observation

def parse_stream_observations(raw_observations):
    """Parses multiple observations from the create stream into a more
    meaningful form.
    """
    observations = _concatenate_observations(raw_observations,
                                             parse_stream_observation)
    for key, value in observations.items():
        if key in ["WheeldropCaster",
                   "WheeldropLeft",
                   "WheeldropRight",
                   "BumpLeftSignal",
                   "BumpRightSignal",
                   "VirtualWall",
                   "LeftWheelOvercurrent",
                   "RightWheelOvercurrent",
                   "IRByte",
                   "RedBuoy",
                   "GreenBuoy",
                   "ForceField",
                   "Reserved",
                   "HomeChargingSourcesAvailable",
                   "InternalChargingSourcesAvailable"]:
            observations[key] = value[- 1]
        if key in ["Distance",
                   "Angle"]:
            observations[key] = sum(value)
        if key in ["BatteryCharge",
                   "WallSignal",
                   "CliffLeftSignal",
                   "CliffFrontLeftSignal",
                   "CliffFrontRightSignal",
                   "CliffRightSignal"]:
            observations[key] = sum(value) / len(value)
    return observations


def parse_sense_observation(raw_observation):
    """Parses one observation from the sense hat stream into a more
    meaningful form.
    """
    observation = dict()
    observation["SenseObservationID"] = raw_observation["observation_id"]
    observation["SenseObservationTime"] = raw_observation["observation_time"]
    observation["temp_h"] = raw_observation["temp_h"]
    observation["temp_p"] = raw_observation["temp_p"]
    observation["humidity"] = raw_observation["humidity"]
    observation["pressure"] = raw_observation["pressure"]
    observation["yaw"] = raw_observation["yaw"]
    observation["pitch"] = raw_observation["pitch"]
    observation["roll"] = raw_observation["roll"]
    observation["compass_x"] = raw_observation["compass_x"]
    observation["compass_y"] = raw_observation["compass_y"]
    observation["compass_z"] = raw_observation["compass_z"]
    observation["accelerometer_x"] = raw_observation["accelerometer_x"]
    observation["accelerometer_y"] = raw_observation["accelerometer_y"]
    observation["accelerometer_z"] = raw_observation["accelerometer_z"]
    observation["gyroscope_x"] = raw_observation["gyroscope_x"]
    observation["gyroscope_y"] = raw_observation["gyroscope_y"]
    observation["gyroscope_z"] = raw_observation["gyroscope_z"]
    return observation


def parse_sense_observations(raw_observations):
    """Parses multiple observations from the sense hat stream into a
    more meaningful form.
    """
    observations = _concatenate_observations(raw_observations,
                                             parse_sense_observation)
    for key, value in observations.items():
        if key not in ["SenseObservationID", "SenseObservationTime"]:
            observations[key] = sum(value) / len(value)
    return observations


def parse_camera_observation(raw_observation):
    """Parses one observation from the camera stream into a more
    meaningful form.
    """
    observation = dict()
    observation["CameraObservationID"] = (
        raw_camera_obs["observation_id"])
    observation["CameraObservationTime"] = (
        raw_camera_obs["observation_time"])
    observation["image"] = (
        raw_camera_obs["image"])
    return observation


def parse_camera_observations(raw_observations):
    """Parses multiple observations from the camera stream into a more
    meaningful form.
    """
    observations = _concatenate_observations(raw_observations,
                                             parse_camera_observation)
    observations["delta"] = (observations["image"][- 1] -
                             observations["image"][0])
    observations["image"] = observations["image"][- 1]
    return observations


def parse_observation(raw_stream_obs,
                      raw_sense_obs=None,
                      raw_camera_obs=None):
    """Parses one observation from the create stream and optionally the
    sense hat stream and/or the camera stream into a more meaningful
    form.
    """
    observation = parse_stream_observation(raw_stream_obs)
    if raw_sense_obs is not None:
        observation.update(parse_sense_observation(raw_sense_obs))
    if raw_camera_obs is not None:
        observation.update(parse_camera_observation(raw_camera_obs))
    return observation


def parse_observations(raw_stream_obs,
                       raw_sense_obs=None,
                       raw_camera_obs=None):
    """Parses multiple observations from the create stream and
    optionally the sense hat stream and/or the camera stream into a more
    meaningful form.
    """
    observations = parse_stream_observations(raw_stream_obs)
    if raw_sense_obs is not None:
        observations.update(parse_sense_observations(raw_sense_obs))
    if raw_camera_obs is not None:
        observations.update(parse_camera_observations(raw_camera_obs))
    return observations
