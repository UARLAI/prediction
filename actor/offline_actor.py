
import math
import numpy as np
import random
import time
import warnings

from .actor import Actor
from .parse_observation import parse_observations
from gvfls.gvfls import GVFLS
from gvfls.tile_coding.tile_coder_adapter import TileCoderAdapter
from logs.actor_log_reader import ActorLogReader
from logs.csv_logger import CSVLogger
from logs.csv_run_log_reader import CSVRunLogReader


class OfflineActor(Actor):
    """Acts on data that has been processed."""
    def __init__(self, args):
        super().__init__(args["batch"], args["clobber"])
        self.actor_log_dir = args["actor_log_dir"]
        self.stream_log_path = args["stream_log_path"]
        self.sense_log_path = args["sense_log_path"]

    def get_next_observation(self):
        """Read the next available observation from the Create."""
        observations = self.actor_log_reader.get_observations()
        if observations == None:
            return None
        return parse_observations(*observations)

    def run(self):
        """Do a series of consecutive runs."""
        self.actor_log_reader = ActorLogReader(
            CSVRunLogReader(self.actor_log_dir, 'actor_log'),
            self.stream_log_path,
            self.sense_log_path)

        i = 0
        while self.actor_log_reader.open_run():
            self.single_run(run_id=i)
            i += 1

    def single_run(self, run_id=0):
        """Begin the action loop."""

        first = True

        # Enter loop
        while not self.actor_log_reader.end_of_run():

            # Tell the actor log reader about the new iteration
            self.actor_log_reader.start_iteration()

            # Get next observation
            observation = self.get_next_observation()

            # Get the logged action
            action = self.actor_log_reader.get_action()

            choices = self.get_choices(observation)
            action_probability = {action: 1 / len(choices)
                                  for action in choices}

            # Set up GVFLS
            if first:
                gvfls = GVFLS(action, 
                              observation, 
                              action_probability, 
                              self.batch, 
                              self.clobber, 
                              run_id)
                first = False

            # Update GVFLS
            gvfls.update(action, observation, action_probability)
