
import multiprocessing as mp

from actor.offline_actor import OfflineActor
from actor.online_actor import OnlineActor
from camera_manager import start_camera_manager
from cspy3 import Controller
from parse_args import parse_args
from sense_manager import start_sense_manager
from stream_manager import start_stream_manager


def main():
    # Parse command line arguments
    args = parse_args()

    if args["offline"]:
        actor = OfflineActor(args).run()
    else:
        # Setup multiprocessing things
        mp.set_start_method("spawn")
        flag = mp.Event()
        try:
            sensors = (7,   # BumpsAndWheelDrops
                       13,  # VirtualWall
                       14,  # Overcurrents
                       17,  # IRByte
                       19,  # Distance
                       20,  # Angle
                       25,  # BatteryCharge
                       27,  # WallSignal
                       28,  # CliffLeftSignal
                       29,  # CliffFrontLeftSignal
                       30,  # CliffFrontRightSignal
                       31,  # CliffRightSignal
                       34,  # ChargingSourcesAvailable
                       41,  # RightVelocity
                       42)  # LeftVelocity

            # Initialize the robot
            control = Controller(args["port"])
            control.mode_full()
            control_lock = mp.Lock()

            # Run stream manager
            stream_output = mp.Queue()
            stream_manager_process = mp.Process(target=start_stream_manager,
                                                name="stream_manager",
                                                args=(flag,
                                                      args["port"],
                                                      control_lock,
                                                      sensors,
                                                      stream_output,
                                                      args["batch"],
                                                      args["clobber"]))
            stream_manager_process.start()

            if args["sense"]:
                # Run sense manager
                sense_output = mp.Queue()
                sense_manager_process = mp.Process(
                                            target=start_sense_manager,
                                            name="sense_manager",
                                            args=(flag,
                                                  args["sense_interval"],
                                                  sense_output,
                                                  args["batch"],
                                                  args["clobber"]))
                sense_manager_process.start()
            else:
                sense_output = None

            if args["camera"]:
                # Run camera manager
                camera_output = mp.Queue()
                camera_manager_process = mp.Process(
                                            target=start_camera_manager,
                                            name="camera_manager",
                                            args=(flag,
                                                  args["camera_interval"],
                                                  camera_output))
                camera_manager_process.start()
            else:
                camera_output = None

            # Make actor
            actor = OnlineActor(args,
                                control,
                                control_lock,
                                stream_output,
                                sense_output,
                                camera_output)

            # Run actor
            actor.run(flag)

        except KeyboardInterrupt:
            pass
        finally:
            # Ensure that the flag is set since program is terminating
            flag.set()

            # Join with processes
            stream_manager_process.join()
            if args["sense"]:
                sense_manager_process.join()
            if args["camera"]:
                camera_manager_process.join()

if __name__ == "__main__":
    main()
