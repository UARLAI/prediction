
import select
import time
import warnings

from logs.csv_logger import CSVLogger
from sense_hat import SenseHat


class SenseManager:
    """Reads streaming data from a raspberry pi sense hat and adds it to
    an output stream. Data is fetched every fetch_interval seconds.
    """
    def __init__(self,
                 fetch_interval,
                 output,
                 batch=None,
                 clobber=False):
        self.batch = batch
        self.clobber = True if self.batch == "default" else clobber
        self.fetch_interval = fetch_interval
        self.output = output
        self.sense_hat = SenseHat()

    def run(self, halt_flag):
        """Begin the stream input processing loop."""
        observation_id = 0
        first_run = True

        while not halt_flag.is_set():
            observation_id += 1
            observation_time = time.time()
            observation = self._fetch_latest_data()
            observation.update({"observation_id": observation_id,
                                "observation_time": observation_time})

            # Log observation
            if self.batch is not None:
                if first_run:
                    header = [item[0] for item in sorted(
                             observation.items())]
                    log = CSVLogger(self.batch,
                                    "sense_observations.csv",
                                    self.clobber,
                                    header=header)
                    first_run = False
                log.write([str(item[1]) for item in sorted(
                           observation.items())])

            self.output.put(observation)

            # Warn the user if too much time has passed
            if time.time() - observation_time > self.fetch_interval:
                warnings.warn("sense data handling time exceeds threshold")

            time.sleep(max(0,
                       self.fetch_interval - (time.time() - observation_time)))

    # Raspberry Pi Foundation; https://www.raspberrypi.org/learning/se \
    # nse-hat-data-logger/worksheet/; 2016-06-07
    def _fetch_latest_data(self):
        data = dict()

        # Temperature
        data["temp_h"] = self.sense_hat.get_temperature_from_humidity()
        data["temp_p"] = self.sense_hat.get_temperature_from_pressure()

        # Humidity
        data["humidity"] = self.sense_hat.get_humidity()

        # Pressure
        data["pressure"] = self.sense_hat.get_pressure()

        # Orientation
        orientation = self.sense_hat.get_orientation()
        data["yaw"] = orientation["yaw"]
        data["pitch"] = orientation["pitch"]
        data["roll"] = orientation["roll"]

        # Compass
        compass = self.sense_hat.get_compass_raw()
        data["compass_x"] = compass["x"]
        data["compass_y"] = compass["y"]
        data["compass_z"] = compass["z"]

        # Accelerometer
        accelerometer = self.sense_hat.get_accelerometer_raw()
        data["accelerometer_x"] = accelerometer["x"]
        data["accelerometer_y"] = accelerometer["y"]
        data["accelerometer_z"] = accelerometer["z"]

        # Gyroscope
        gyroscope = self.sense_hat.get_gyroscope_raw()
        data["gyroscope_x"] = gyroscope["x"]
        data["gyroscope_y"] = gyroscope["y"]
        data["gyroscope_z"] = gyroscope["z"]

        return data


def start_sense_manager(flag, fetch_interval, output, batch, clobber):
    """Creates and starts a new SenseManager."""
    try:
        SenseManager(fetch_interval,
                     output,
                     batch=batch,
                     clobber=clobber).run(flag)
    finally:
        flag.set()
